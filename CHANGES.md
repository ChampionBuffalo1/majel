# Change Log

## 0.2.4 (Bajoran)

* Fixed a logo rendering problem on the home page.


## 0.2.3 (Bajoran)

* Added a proper logo.
* Removed the rotating wallpaper from the home page.


## 0.2.2 (Bajoran)

* Tweaked the Amazon handler to widen the range of URLs it'll claim.
* Fixed the click event on the Amazon handler so now it actually plays the
  video when you get to the page.
* Refactored the way we detect whether a browser action is "noisy".  The
  functionality hasn't changed, but now there's a nice way to override how that
  detection works should someone want to.
* Added more logging to make it easier to understand what's going on.
* Added a demo video to the `README`.


## 0.2.1 (Bajoran)

* Added this file!
* Fixed the handling of a race condition when Mycroft sends a `stop` signal
  over the message bus for one service and a `play` signal for another one.
* Added a click event to auto-play Netflix streams.
* Added some more debugging information


## 0.2.0 (Bajoran)

* Switch to Firefox for the browser actions.  I was never happy depending on
  Google tech for a Free software project, and since we're using Firefox's
  bookmarks anyway, this only made sense.
* Drop the use of environment variables in favour of `/etc/mycroft.yml`.
* Update dependencies.


## 0.1.1 (Andorian)

* Cosmetic updates for PyPI


## 0.1.0 (Andorian)

* Initial release
