#
# These must be here to ensure that `Action.__subclasses__()` returns them
#

from .browser import BrowserAction
from .kodi import KodiAction
